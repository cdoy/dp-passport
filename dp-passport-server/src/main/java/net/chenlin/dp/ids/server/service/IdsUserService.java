package net.chenlin.dp.ids.server.service;

import net.chenlin.dp.ids.server.entity.IdsUserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户api
 * @author zcl<yczclcn@163.com>
 */
@Mapper
public interface IdsUserService {

    /**
     * 根据用户名查询用户信息
     * @param username
     * @return
     */
    IdsUserEntity getByUserName(String username);

    /**
     * 更新用户最近登录时间
     * @param userId
     * @return
     */
    Integer updateUserLastLoginTime(Long userId);

}
