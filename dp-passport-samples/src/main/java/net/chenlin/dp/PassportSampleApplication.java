package net.chenlin.dp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 启动器
 * @author zcl<yczclcn@163.com>
 */
@SpringBootApplication
@ComponentScan(basePackages = "net.chenlin")
public class PassportSampleApplication {

    private static final Logger logger = LoggerFactory.getLogger(PassportSampleApplication.class);

    /**
     * jar启动
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(PassportSampleApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
        logger.info("The dp passport sample has been started successfully!");
    }

}
