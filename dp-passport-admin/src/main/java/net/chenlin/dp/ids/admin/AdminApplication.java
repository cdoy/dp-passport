package net.chenlin.dp.ids.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * app启动器
 * @author zhouchenglin[yczclcn@163.com]
 */
@SpringBootApplication
public class AdminApplication {

    private static final Logger logger = LoggerFactory.getLogger(AdminApplication.class);

    /**
     * jar启动
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(AdminApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
        logger.info("The dp passport admin has been started successfully!");
    }

}
